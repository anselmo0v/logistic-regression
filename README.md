# Machine Learning Engineering Assignment

This is a working solution for the deployment of the given model. The code is divided into preprocessing, inference (model.py) and postprocessing steps. 

The .yml file contains the code for the pipeline that:

    1. Creates the docker image from the Dockerfile.

    2. Pushes the created image to the DockerHub repository.

    3. In a complete cloud solution, it updates the service on use with the new docker image.

The Dockerfile contains the code needed to create a container that runs the model as a service waiting for requests to attend. Fastapi and uvicorn are used for this purpose.

The example data .csv file is placed inside the container. This is assumed to be the case in a complete cloud solution where the input data for a request is provided in a data source it needs to be downloaded from.

### Running the solution:

    * Start the docker daemon in your local machine

    * Download the docker image for production environment

    * docker pull anselmo0v/logistic-regression:prod-v1.0.0

    * Run the docker container for the image
	docker run -d <image id>

    * Open shell inside the running container 
	docker exec -it <container id> /bin/bash

    * Use the tool of your preference to send a GET http request to http://127.0.0.1:8000

    * The inference result can be seen in the response object
