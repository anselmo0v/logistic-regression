FROM python:3.10

EXPOSE 8000

# Copy function code
COPY preprocessing.py .
COPY model.py .
COPY postprocessing.py .
COPY main.py .

# Install the function's dependencies using file requirements.txt
# from your project folder.

COPY dataset.csv .
COPY requirements.txt  .
RUN pip install -r requirements.txt --progress-bar off

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "uvicorn", "main:app", "--reload" ]